package demoapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("http://localhost:8080")
@RestController
public class PageController {

    @GetMapping("/test")
    public String uploadFile1() {
        System.out.println("----------------------test---------------------");
        return "1234 test";
    }
}
